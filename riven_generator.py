import random
import sys
from data_raw import *
from data_dict import *
import copy
class user_riven:
    def __init__(self, type, wep, pos, neg, keys, rolls, mr, pol):
        self._type = type
        self._weapon = wep
        self._pos = pos
        self._neg = neg
        self._stat_keys = keys
        self._rolls = rolls
        self._mr = mr
        self._pol = pol
    def get_type(self):
        return self._type
    def get_type_string(self):
        return types_by_id[self._type]
    def get_weapon(self):
        return self._weapon
    def get_weapon_string(self):
        return weapons[self._type][self._weapon][0]
    def get_pos(self):
        return self._pos
    def get_neg(self):
        return self._neg
    def get_rolls(self):
        return self._rolls
    def get_mr(self):
        return self._mr
    def get_pol(self):
        return self._pol
    def get_stat_range(self, key, neg):
        #+++, +++-, ++, ++-
        posWeight = (0.75, 0.9375, 0.99, 1.2375)
        #+++, ++
        negWeight = (0, 0.75, 0, 0.495)
        if key is None:
            return None
        else:
            weapon = weapons[self._type][self._weapon]
            type_stat_table = weapon_type_weights[self._type]
            stat = type_stat_table[key]
            base = round(stat[0] * weapon[1], 2)
            if neg:
                base = -base * negWeight[self.get_pn_mod()]
            else:
                base = base * posWeight[self.get_pn_mod()]
            
            var = base * 0.1
            max = round(base + var, 2)
            min = round(base - var, 2)
            return [min, max]
    def get_pn_mod(self):
        #0:+++ 1:+++- 2:++ 3:++-
        if sum(x is not None for x in self._pos) == 3:
            if self._neg is not None:
                #+++-
                return 1
            else:
                #+++
                return 0
        else:
            if self._neg is not None:
                #++-
                return 3
            else:
                #++
                return 2
    def get_stat_var(self, stat, stat_key, neg):
    
        if stat is None or stat_key is None:
            return None
        else:
            base = round(weapon_type_weights[self._type][stat_key][0] * weapons[self._type][self._weapon][1], 2)
            
            if neg:
                base = -base * negWeight[self.get_pn_mod()]
            else:
                base = base * posWeight[self.get_pn_mod()]
            #print((stat/base))
            return round(1-(stat/base), 3)*-1
    def gen_name(self):
        pre = None
        core = None
        suf = None
        if sum(x is not None for x in self._pos) == 3:
            # print("3 pos")
            stat_var = [
                            [self.get_stat_var(self._pos[0], self._stat_keys[0], False), self._stat_keys[0]],
                            [self.get_stat_var(self._pos[1], self._stat_keys[1], False), self._stat_keys[1]],
                            [self.get_stat_var(self._pos[2], self._stat_keys[2], False), self._stat_keys[2]]
                        ]
            stat_var = sorted(stat_var)
            suf = stat_var[0][1]
            core = stat_var[1][1]
            pre = stat_var[2][1]
            # print("Pre: "+str(weapon_type_weights[self._type][pre][1]))
            # if core == None:
                # print("Core: "+str(core))
            # else:
                # print("Core: "+str(weapon_type_weights[self._type][core][1]))
            # print("Suf: "+str(weapon_type_weights[self._type][suf][1]))
        else:
            # print("less than 3 pos")
            stat_var = [
                            [self.get_stat_var(self._pos[0], self._stat_keys[0], False), self._stat_keys[0]],
                            [self.get_stat_var(self._pos[1], self._stat_keys[1], False), self._stat_keys[1]]
                        ]
            stat_var = sorted(stat_var)
            suf = stat_var[0][1]
            pre = stat_var[1][1]
            
            # print("Pre: "+str(weapon_type_weights[self._type][pre][1]))
            # print("Suf: "+str(weapon_type_weights[self._type][suf][1]))
        
        unique_name = ""
        if core is None:
            pre = naming_fix[weapon_type_weights[self._type][pre][1]]
            suf  = naming_fix[weapon_type_weights[self._type][suf][1]]
            unique_name = str(pre[0]+suf[1].lower())
        else:
            pre = naming_fix[weapon_type_weights[self._type][pre][1]]
            core = naming_fix[weapon_type_weights[self._type][core][1]]
            suf  = naming_fix[weapon_type_weights[self._type][suf][1]]
            unique_name = str(pre[0])+"-"+str(core[0]+suf[1].lower())
        
        return(self.get_weapon_string()+" "+unique_name)    
    def to_string(self):
        if self._weapon == -1:
            print("Unveiled "+self.get_type_string()+" Riven Mod")
        else:
            print("**********************")
            print(self.gen_name())
            for i, stat in enumerate(self._stat_keys):
                if stat is not None:
                    stat_arr = weapon_type_weights[self.get_type()][stat]
                    if(stat_arr[1] == "Weapon Recoil" and not i == 3):
                        print(str(self._pos[i])+"% "+stat_arr[1] + " -> " +str(self.get_stat_var(self._pos[i], stat, False)) )
                    elif(stat_arr[1] == "Weapon Recoil" and i == 3):
                        print("+"+str(self._neg)+"% "+stat_arr[1] + " -> " +str(self.get_stat_var(self._neg, stat, True)))
                    elif i == 3:
                        print(str(self._neg)+"% "+stat_arr[1] + " -> " +str(self.get_stat_var(self._neg, stat, True)))
                    else:
                        print("+"+str(self._pos[i])+"% "+stat_arr[1] + " -> " +str(self.get_stat_var(self._pos[i], stat, False)))
            print("**********************")

class riven_mod:
    def __init__(self, type = random.choice(list(weapons.keys()))):

        self._type = type               #min: 0, max: 6; 3 bits
        self._weapon = -1               #min: 0, max: currently 113; 8 bits should do, until we get more than 256 of a given weapon type.
        self._pos = [None] * 3          #8 bits per value, only store variance * 1000
        self._neg = [None]              #8 bits per value, only store variance * 1000
        self._stat_keys = [None] * 4    #min: 0, max: 23; 5 bits should be sufficient.
        self._rolls = 0                 #min:0, max: unknown; 16 bits to be safe
        self._mr = 0                    #min: 8, max: 16; 4 bits
        self._pol = 0                   #min: 0, max: 2; 2 bits
                
    def get_type(self):
        return self._type
    def get_type_string(self):
        return types_by_id[self._type]
    def get_weapon(self):
        return self._weapon
    def get_weapon_string(self):
        return weapons[self._type][self._weapon][0]
    def get_pos(self):
        return self._pos
    def get_neg(self):
        return self._neg
    def get_rolls(self):
        return self._rolls
    def get_mr(self):
        return self._mr
    def get_pol(self):
        return self._pol
    
    def print_binary(self):
        print("Type: "+str(self._type))
        print(format(self._type,'b'))
        
        print("Weapon: "+str(self._weapon))
        print(format(self._weapon,'b'))
        
        #print("Positive Stats:")
        #for i, pos in enumerate(self._pos):
            #var_format = self.get_stat_var(self._pos, i, False)
            #if(pos is None):
                #print("\tPositive Stat "+str(i)+" : "+str(var_format))
            #else:
                #print("\tPositive Stat "+str(i)+" : "+str(var_format))
                #print("\t"+format(int(var_format),'b'))
        
        #if(self._neg is None):
            #print("Negative "+str(i)+" : "+str(self._neg))
        #else:
           # print("Negative "+str(self._neg)+":")
            #print(format(int(self._neg*10),'b'))
        
        print("Stat Keys: ")
        for i, stat in enumerate(self._stat_keys):
            if stat is None:
                print("\tStat Key "+str(i)+" : "+str(None))
            else:
                if i == 3:
                    var_format = self.get_stat_var(self._neg, stat, True)*1000
                else:
                    var_format = self.get_stat_var(self._pos[i], stat, False)*1000
                
                if var_format < 0:
                    signed_bit = 1
                else:
                    signed_bit = 0
                
                var_format = int(abs(var_format))
                print("\tStat Key "+str(i)+" : "+str(var_format))
                print("\t"+str(signed_bit)+" "+format(var_format,'b'))
        
        print("Rolls: "+str(self._rolls))
        print(format(self._rolls,'b'))
        
        print("Mastery: "+str(self._mr))
        print(format(self._mr,'b'))
        
        print("Polarity: "+str(self._pol))
        print(format(self._pol,'b'))
    
    def get_pn_mod(self):
        #0:+++ 1:+++- 2:++ 3:++-
        if sum(x is not None for x in self._pos) == 3:
            if self._neg is not None:
                #+++-
                return 1
            else:
                #+++
                return 0
        else:
            if self._neg is not None:
                #++-
                return 3
            else:
                #++
                return 2
        
    def get_stat_var(self, stat, stat_key, neg):
    
        if stat is None or stat_key is None:
            return None
        else:
            base = round(weapon_type_weights[self._type][stat_key][0] * weapons[self._type][self._weapon][1], 2)
            
            if neg:
                base = -base * negWeight[self.get_pn_mod()]
            else:
                base = base * posWeight[self.get_pn_mod()]
            #print((stat/base))
            return round(1-(stat/base), 3)*-1
    
    def gen_name(self):
        pre = None
        core = None
        suf = None
        if sum(x is not None for x in self._pos) == 3:
            # print("3 pos")
            stat_var = [
                            [self.get_stat_var(self._pos[0], self._stat_keys[0], False), self._stat_keys[0]],
                            [self.get_stat_var(self._pos[1], self._stat_keys[1], False), self._stat_keys[1]],
                            [self.get_stat_var(self._pos[2], self._stat_keys[2], False), self._stat_keys[2]]
                        ]
            stat_var = sorted(stat_var)
            suf = stat_var[0][1]
            core = stat_var[1][1]
            pre = stat_var[2][1]
            # print("Pre: "+str(weapon_type_weights[self._type][pre][1]))
            # if core == None:
                # print("Core: "+str(core))
            # else:
                # print("Core: "+str(weapon_type_weights[self._type][core][1]))
            # print("Suf: "+str(weapon_type_weights[self._type][suf][1]))
        else:
            # print("less than 3 pos")
            stat_var = [
                            [self.get_stat_var(self._pos[0], self._stat_keys[0], False), self._stat_keys[0]],
                            [self.get_stat_var(self._pos[1], self._stat_keys[1], False), self._stat_keys[1]]
                        ]
            stat_var = sorted(stat_var)
            suf = stat_var[0][1]
            pre = stat_var[1][1]
            
            # print("Pre: "+str(weapon_type_weights[self._type][pre][1]))
            # print("Suf: "+str(weapon_type_weights[self._type][suf][1]))
        
        unique_name = ""
        if core is None:
            pre = naming_fix[weapon_type_weights[self._type][pre][1]]
            suf  = naming_fix[weapon_type_weights[self._type][suf][1]]
            unique_name = str(pre[0]+suf[1].lower())
        else:
            pre = naming_fix[weapon_type_weights[self._type][pre][1]]
            core = naming_fix[weapon_type_weights[self._type][core][1]]
            suf  = naming_fix[weapon_type_weights[self._type][suf][1]]
            unique_name = str(pre[0])+"-"+str(core[0]+suf[1].lower())
        
        return(self.get_weapon_string()+" "+unique_name)
        
    def to_string(self):
        if self._weapon == -1:
            print("Unveiled "+self.get_type_string()+" Riven Mod")
        else:
            print("**********************")
            print(self.gen_name())
            for i, stat in enumerate(self._stat_keys):
                if stat is not None:
                    stat_arr = weapon_type_weights[self.get_type()][stat]
                    if(stat_arr[1] == "Weapon Recoil" and not i == 3):
                        print(str(self._pos[i])+"% "+stat_arr[1] + " -> " +str(self.get_stat_var(self._pos[i], stat, False)) )
                    elif(stat_arr[1] == "Weapon Recoil" and i == 3):
                        print("+"+str(self._neg)+"% "+stat_arr[1] + " -> " +str(self.get_stat_var(self._neg, stat, True)))
                    elif i == 3:
                        print(str(self._neg)+"% "+stat_arr[1] + " -> " +str(self.get_stat_var(self._neg, stat, True)))
                    else:
                        print("+"+str(self._pos[i])+"% "+stat_arr[1] + " -> " +str(self.get_stat_var(self._pos[i], stat, False)))
            print("**********************")

class riven_data:
    def __init__(self, riven=None):
        if(riven != None):
            #print((int(format(riven._type,'b')).bit_length()))
            #print("0"*(3-len(format(riven._type,'b'))) )
            self.__type = "0"*(3-len(format(riven._type,'b'))) + format(riven._type,'b')    #min: 0, max: 6; 3 bits
            self.__weapon = "0"*(8-len(format(riven._weapon,'b'))) + format(riven._weapon,'b')  #min: 0, max: currently 113; 8 bits should do, until we get more than 256 of a given weapon type.
            self.__stat_var = [None]*4  #8 bits per value, only store variance * 1000
            self.__stat_keys = [None]*4 #min: 0, max: 23; 5 bits should be sufficient.
            self._rolls = "0"*(12-len(format(riven._rolls,'b'))) + format(riven._rolls,'b') #min:0, max: unknown; 12 bits to be safe
            self._mr = "0"*(3-len(format(riven._mr,'b'))) + format(riven._mr,'b')#min: 8, max: 16; 3 bits
            self._pol = "0"*(2-len(format(riven._pol,'b'))) + format(riven._pol,'b')#min: 0, max: 2; 2 bits                                          
            for i, stat in enumerate(riven._stat_keys):
                if stat is None:
                    #All 1's indicate no value
                    self.__stat_keys[i] = format(31,'b')
                    self.__stat_var[i] = format(1,'b') + format(127,'b')
                else:
                    if i == 3:
                        var_format = riven.get_stat_var(riven._neg, stat, True)*1000
                    else:
                        var_format = riven.get_stat_var(riven._pos[i], stat, False)*1000
                    
                    if var_format < 0:
                        signed_bit = 1
                    else:
                        signed_bit = 0
                    
                    var_format = int(abs(var_format))
                    
                    var_leading_bits = "0"*(7-len(format(var_format,'b')))
                    key_leading_bits = "0"*(5-len(format(stat,'b')))
                    
                    self.__stat_keys[i] = key_leading_bits + format(stat,'b')
                    self.__stat_var[i] = format(signed_bit,'b') + var_leading_bits + format(var_format,'b')
        else:
            self.__type = "000"
            self.__weapon = "00000000"
            self.__stat_var = ["00000000","00000000","00000000","00000000"]
            self.__stat_keys = ["00000","00000","00000","00000"]
            self._rolls = "000000000000"
            self._mr = "000"
            self._pol = "00"
    
    #binary string gets
    def get_type_bin(self):
        return self.__type
    def get_weapon_bin(self):
        return self.__weapon
    def get_stat_keys_bin(self, i):
        return self.__stat_keys[i]
    def get_stat_var_bin(self, i):
        return self.__stat_var[i]
    def get_rolls_bin(self):
        return self._rolls
    def get_mr_bin(self):
        return self._mr
    def get_pol_bin(self):
        return self._pol
    
    #decimal string gets
    def get_type_dec_str(self):
        return str(int(self.__type,2))
    def get_weapon_dec_str(self):
        return str(int(self.__weapon,2))
    def get_stat_keys_dec_str(self, i):
        if self.__stat_keys[i] == "11111":
            return "None"
        else:
            return str(int(self.__stat_keys[i],2))
    def get_stat_var_dec_str(self, i):
        if self.__stat_var[i] == "11111111":
            return "None"
        elif self.__stat_var[i][0] == "1":
            return "-"+str(int(self.__stat_var[i][1:],2)/1000)
        else:
            return "+"+str(int(self.__stat_var[i][1:],2)/1000)
    def get_rolls_dec_str(self):
        return str(int(self._rolls,2))
    def get_mr_dec(self):
        return str(int(self._mr,2)+8)
    def get_pol_dec_str(self):
        return str(int(self._pol,2))
    
    #decimal gets
    def get_type_dec_int(self):
        return int(self.__type,2)
    def get_weapon_dec_int(self):
        return int(self.__weapon,2)
    def get_stat_keys_dec_int(self, i):
        if self.__stat_keys[i] == "11111":
            return "None"
        else:
            return int(self.__stat_keys[i],2)
    def get_stat_var_dec_int(self, i):
        if self.__stat_var[i] == "11111111":
            return "None"
        elif self.__stat_var[i][0] == "1":
            return -int(self.__stat_var[i][1:],2)/1000
        else:
            return int(self.__stat_var[i][1:],2)/1000
    def get_rolls_dec_int(self):
        return int(self._rolls,2)
    def get_mr_dec_int(self):
        return int(self._mr,2)
    def get_pol_dec_int(self):
        return int(self._pol,2)
    def to_string(self):
        print("Type: "+str(self.__type)+" -> "+self.get_type_dec_str())
        print("Weapon: "+str(self.__weapon)+" -> "+self.get_weapon_dec_str())
        for i, stat in enumerate(self.__stat_keys):
            print("Stat key "+str(i)+": "+str(stat)+" -> "+self.get_stat_keys_dec_str(i))
        for i, stat in enumerate(self.__stat_var):
            print("Stat "+str(i)+": "+str(stat)+" -> "+self.get_stat_var_dec_str(i))
        print("Rolls: "+str(self._rolls)+" -> "+self.get_rolls_dec_str())
        print("MR: "+str(self._mr)+" -> "+self.get_mr_dec())
        print("Polarity: "+str(self._pol)+" -> "+self.get_pol_dec_str())
    def build_packet(self):
        packet = self.get_type_bin()+self.get_weapon_bin()+self.get_stat_keys_bin(0)+self.get_stat_keys_bin(1)+self.get_stat_keys_bin(2)+self.get_stat_keys_bin(3)+self.get_stat_var_bin(0)+self.get_stat_var_bin(1)+self.get_stat_var_bin(2)+self.get_stat_var_bin(3)+self.get_rolls_bin()+self.get_mr_bin()+self.get_pol_bin()
        return packet
    def get_packet_size(self):
        packet = self.build_packet()
        return len(packet)
    def token_packet(self, p):
        packet = p
        index = len(self.get_type_bin())
        packet = packet[:index] + " " + packet[index:] #type: 3 bits
        index += len(self.get_weapon_bin())+1
        packet = packet[:index] + " " + packet[index:] #weapon: 8 bits
        index += len(self.get_stat_keys_bin(0))+1
        packet = packet[:index] + " " + packet[index:] #stat key 0: 5 bits
        index += len(self.get_stat_keys_bin(1))+1
        packet = packet[:index] + " " + packet[index:] #stat key 1: 5 bits
        index += len(self.get_stat_keys_bin(2))+1
        packet = packet[:index] + " " + packet[index:] #stat key 2: 5 bits
        index += len(self.get_stat_keys_bin(3))+1
        packet = packet[:index] + " " + packet[index:] #stat key 3: 5 bits
        index += len(self.get_stat_var_bin(0))+1
        packet = packet[:index] + " " + packet[index:] #stat var: 8 bits
        index += len(self.get_stat_var_bin(1))+1
        packet = packet[:index] + " " + packet[index:] #stat var: 8 bits
        index += len(self.get_stat_var_bin(2))+1
        packet = packet[:index] + " " + packet[index:] #stat var: 8 bits
        index += len(self.get_stat_var_bin(3))+1
        packet = packet[:index] + " " + packet[index:] #stat var: 8 bits
        index += len(self.get_rolls_bin())+1
        packet = packet[:index] + " " + packet[index:] #rolls: 12 bits
        index += len(self.get_mr_bin())+1
        packet = packet[:index] + " " + packet[index:] #MR: 4 bits
        index += len(self.get_pol_bin())+1
        packet = packet[:index] + " " + packet[index:] #polarity: 2 bits
        return packet.split()
    def init_packet(self, p):
        packet = p
        packet = self.token_packet(packet)
        
        self.__type =           packet[0]
        self.__weapon =         packet[1]
        self.__stat_keys[0] =   packet[2]
        self.__stat_keys[1] =   packet[3]
        self.__stat_keys[2] =   packet[4]
        self.__stat_keys[3] =   packet[5]
        self.__stat_var[0] =    packet[6]
        self.__stat_var[1] =    packet[7]
        self.__stat_var[2] =    packet[8]
        self.__stat_var[3] =    packet[9]
        self._rolls =           packet[10]
        self._mr =              packet[11]
        self._pol =             packet[12]
    def print_packet_tokens(self, p):
        for t in p:
            print(t+" ", end = '')
        print("\n")   
class riven_generator:
    def __init__(self):
        #+++, +++-, ++, ++-
        self.__posWeight = (0.75, 0.9375, 0.99, 1.2375)
        #+++, ++
        self.__negWeight = (0, 0.75, 0, 0.495)
    
    def unveil_riven(self, riven):
        if riven.get_weapon() == -1:
            riven._weapon = random.choice(list(weapons[riven.get_type()].keys()))
            riven._mr = random.randint(0,7)
            riven._pol = random.choice(list(polarity.keys()))
            self.roll_riven(riven, True)
        else:
            print("This riven has already been unveiled.")
    
    def randomize_stat(self, riven, pn_mod, neg):
        weapon = weapons[riven.get_type()][riven.get_weapon()]
        type_stat_table = weapon_type_weights[riven.get_type()]
        new_stat_key = random.choice(list(type_stat_table.keys()))
        if(neg):
            while((type_stat_table[new_stat_key][2] == False)or(new_stat_key in riven._stat_keys)):
                new_stat_key = random.choice(list(type_stat_table.keys()))
            new_stat = type_stat_table[new_stat_key]
        else:
            while new_stat_key in riven._stat_keys:
                new_stat_key = random.choice(list(type_stat_table.keys()))
            new_stat = type_stat_table[new_stat_key]
        
        base = round(new_stat[0] * weapon[1], 2)
        
        if neg:
            base = -base * negWeight[pn_mod]
        else:
            base = base * posWeight[pn_mod]
        
        base_var = base * 0.1
        random_var = round(random.uniform(-base_var, base_var), 2)
        
        final_stat = round(base + random_var, 2)
        
        return [new_stat_key, final_stat]
    
    def roll_riven(self, riven, first=False):
        pn_mod = random.randint(0,3)
        old_riven = copy.deepcopy(riven)
        #Has negative
        if pn_mod == 1 or pn_mod == 3:
            if pn_mod == 1: #+++-
                for i, p_stat in enumerate(riven._pos):
                    stat_arr = self.randomize_stat(riven, pn_mod, False)
                    riven._stat_keys[i] = stat_arr[0]
                    riven._pos[i] = stat_arr[1]
                n_stat = self.randomize_stat(riven, pn_mod, True)
                riven._stat_keys[3] = n_stat[0]
                riven._neg = n_stat[1]
            else: #++-
                for i, p_stat in enumerate(riven._pos):
                    if i == len(riven._pos)-1:
                        break
                    stat_arr = self.randomize_stat(riven, pn_mod, False)
                    riven._stat_keys[i] = stat_arr[0]
                    riven._pos[i] = stat_arr[1]
                n_stat = self.randomize_stat(riven, pn_mod, True)
                riven._stat_keys[3] = n_stat[0]
                riven._neg = n_stat[1]
                riven._pos[2] = None
        else:
            if pn_mod == 0: #+++
                for i, p_stat in enumerate(riven._pos):
                    stat_arr = self.randomize_stat(riven, pn_mod, False)
                    riven._stat_keys[i] = stat_arr[0]
                    riven._pos[i] = stat_arr[1]
            else: #++
                for i, p_stat in enumerate(riven._pos):
                    if i == len(riven._pos)-1:
                        break
                    stat_arr = self.randomize_stat(riven, pn_mod, False)
                    riven._stat_keys[i] = stat_arr[0]
                    riven._pos[i] = stat_arr[1]
                riven._pos[2] = None
            riven._neg = None
            
        if first:
            print("Unveiled Riven: ")
            riven.to_string()
            print("")
        else:
            print("OLD Riven:")
            old_riven.to_string()
            print("\nNEW Riven:")
            riven.to_string()
            
            inp = input("Type OLD to keep original roll, or NEW to keep new roll: ")
            valid = False
            while not valid:
                if inp.lower() == "new":
                    print("You chose: ")
                    riven.to_string()
                    inp = input("Are you sure? Type YES or NO: ")
                    yn_valid = False
                    while not yn_valid:
                        if inp.lower() == "yes":
                            print("Roll locked in.")
                            yn_valid = True
                            valid = True
                        elif inp.lower() == "no":
                            yn_valid = True
                            inp = input("Type OLD to keep original roll, or NEW to keep new roll: ")
                        else:
                            print("INVALID INPUT")
                            yn_valid = False 
                elif inp.lower() == "old":
                    print("You chose: ")
                    old_riven.to_string()
                    inp = input("Are you sure? Type YES or NO: ")
                    yn_valid = False
                    while not yn_valid:
                        if inp.lower() == "yes":
                            print("Roll locked in.")
                            riven = old_riven
                            yn_valid = True
                            valid = True
                        elif inp.lower() == "no":
                            yn_valid = True
                            inp = input("Type OLD to keep original roll, or NEW to keep new roll: ")
                        else:
                            print("INVALID INPUT")
                            yn_valid = False 
                else:
                    print("INVALID INPUT...")
                    valid = False
                    inp = input("Type OLD to keep original roll, or NEW to keep new roll: ")
            
            riven._rolls += 1
            print("Your Riven: ")
            riven.to_string()

riven = riven_mod()

server = riven_generator()
server.unveil_riven(riven)

binary = riven_data(riven)
packet = binary.build_packet()
packet = binary.token_packet(packet)
binary.print_packet_tokens(packet)

riven_clone = user_riven(riven._type, riven._weapon, riven._pos, riven._neg, riven._stat_keys, riven._rolls, riven._mr, riven._pol)

print(riven_clone.get_stat_range(riven._stat_keys[0], False))
print(riven_clone.get_stat_range(riven._stat_keys[1], False))
print(riven_clone.get_stat_range(riven._stat_keys[2], False))

print(riven_clone.get_stat_range(riven._stat_keys[3], True))

#empty = riven_data(None)
#binary = riven_data(riven)
#binary.to_string()

#packet = binary.build_packet()
#print(packet)
#print("bits in packet: "+str(binary.get_packet_size()))

#empty.init_packet(packet)
#empty.to_string()

#riven.print_binary()
#server.roll_riven(riven)
